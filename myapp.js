Messages = new Meteor.Collection("Messages")

if (Meteor.isClient) {
  Template.hello.greeting = function () {
    return "Welcome to myapp.";
  };

  Template.hello.events({
    'click input' : function () {
      // template data, if any, is available in 'this'
      if (typeof console !== 'undefined')
        console.log("You pressed the button");
    }
  });
  
  Template.messages.messages = function() {
	return Messages.find().fetch();
  }
  
  Template.messages.events({
	  'click a.remove' : function (evt) {
		evt.preventDefault();
	    Messages.remove({_id: this._id});
	  },
	  'keypress input.add-message' : function(evt) {
	    if (evt.which == 13) {
		    //var message = template.find('.add-message').value;
			Messages.insert({message : evt.srcElement.value});
			//this.find('input.add-message').focus();
			//evt.srcElement.focus();
		}
	  }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
